package com.github.aminmsvi.movio.utils

import android.content.Intent
import android.net.Uri
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.BindingAdapter
import com.devs.readmoreoption.ReadMoreOption
import com.github.aminmsvi.movio.GlideApp
import com.github.aminmsvi.movio.R
import com.github.aminmsvi.movio.model.Movie
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author aminmsvi
 * @since
 */

@BindingAdapter("srcUrl")
fun ImageView.bindSrcUrl(url: String) {
	GlideApp.with(this)
		.load(url)
		.placeholder(R.color.placeholder)
		.error(R.color.placeholder)
		.into(this)
}

@BindingAdapter("formatReleaseDate")
fun TextView.bindFormatReleaseDate(date: String) {
	try {
		val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
		dateFormat.timeZone = TimeZone.getTimeZone("UTC")
		text = SimpleDateFormat("dd MMM yyyy", Locale.US).format(dateFormat.parse(date) ?: "")
	} catch (ignore: Exception) {
		text = ""
	}
}

@BindingAdapter("rating")
fun RatingBar.bindRating(rate: Float) {
	rating = rate.map(0, 10, 0, 5)
}

@BindingAdapter("hyperlink")
fun TextView.bindHyperlink(url: String) {
	if (text.isNullOrEmpty()) return
	setOnClickListener {
		val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
		startActivity(context, browserIntent, null)
	}
}

@BindingAdapter("expandableText")
fun TextView.bindExpandableText(text: String?) {
	if (text.isNullOrEmpty()) return
	val readMoreOption = ReadMoreOption.Builder(context)
		.textLength(4, ReadMoreOption.TYPE_LINE)
		.moreLabel("show more")
		.lessLabel("less")
		.moreLabelColor(ContextCompat.getColor(context, R.color.color_secondary))
		.lessLabelColor(ContextCompat.getColor(context, R.color.color_secondary))
		.labelUnderLine(true)
		.expandAnimation(true)
		.build()

	readMoreOption.addReadMoreTo(this, text)
}

@BindingAdapter("genres")
fun TextView.bindGenres(genres: List<Movie.Genre>) {
	val builder = StringBuilder()
	genres.forEachIndexed { index, genre ->
		if (index > 2) return@forEachIndexed
		builder.append(genre.name)
			.append(" / ")
	}
	text = builder.trimEnd(' ', '/')
}

