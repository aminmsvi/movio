package com.github.aminmsvi.movio.utils

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * @author aminmsvi
 * @since
 */
class AuthInterceptor(
	private val apiKey: String
) : Interceptor {
	override fun intercept(chain: Interceptor.Chain): Response {
		val original: Request = chain.request()

		val url = chain.request().url.newBuilder()
			.addQueryParameter("api_key", apiKey)
			.build()

		val request = original.newBuilder().url(url).build()
		return chain.proceed(request)
	}
}