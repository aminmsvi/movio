package com.github.aminmsvi.movio.ui.movielist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.aminmsvi.movio.databinding.ErrorItemBinding
import com.github.aminmsvi.movio.databinding.MovieItemBinding
import com.github.aminmsvi.movio.databinding.ProgressItemBinding
import com.github.aminmsvi.movio.model.MovieListItem

/**
 * @author aminmsvi
 * @since
 */
class MovieAdapter(
	private val clickListener: ItemClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

	private val items = arrayListOf<MovieListItem>()
	private val isLoading: Boolean
		get() = items.lastOrNull()?.id == ViewType.PROGRESS.typeId
	private val isError: Boolean
		get() = items.lastOrNull()?.id == ViewType.ERROR.typeId

	override fun getItemCount(): Int = items.size

	override fun getItemViewType(position: Int): Int {
		return when (items[position].id) {
			ViewType.PROGRESS.typeId -> ViewType.PROGRESS.typeId
			ViewType.ERROR.typeId -> ViewType.ERROR.typeId
			else -> ViewType.MOVIE.typeId
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		return when (viewType) {
			ViewType.MOVIE.typeId -> MovieViewHolder(
				MovieItemBinding.inflate(
					LayoutInflater.from(parent.context),
					parent,
					false
				)
			)
			ViewType.PROGRESS.typeId -> ProgressViewHolder(
				ProgressItemBinding.inflate(
					LayoutInflater.from(parent.context),
					parent,
					false
				)
			)
			ViewType.ERROR.typeId -> ErrorViewHolder(
				ErrorItemBinding.inflate(
					LayoutInflater.from(parent.context),
					parent,
					false
				)
			)
			else -> throw IllegalArgumentException()
		}
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		when (holder) {
			is MovieViewHolder -> holder.bind(items[position], clickListener)
			is ProgressViewHolder -> holder.bind()
			is ErrorViewHolder -> holder.bind(clickListener)
			else -> {
			}
		}
	}

	fun addLoading() {
		if (isLoading) return
		items.add(MovieListItem(id = ViewType.PROGRESS.typeId))
		notifyItemInserted(items.lastIndex)
	}

	fun removeLoading() {
		if (!isLoading) return
		items.removeAt(items.lastIndex)
		notifyItemRemoved(items.lastIndex + 1)
	}

	fun showError() {
		if (isError) return
		items.add(MovieListItem(id = ViewType.ERROR.typeId))
		notifyItemInserted(items.lastIndex)
	}

	fun hideError() {
		if (!isError) return
		items.removeAt(items.lastIndex)
		notifyItemRemoved(items.lastIndex + 1)
	}

	fun submitChange(change: ListChange) {
		when (change) {
			is ListChangeCleared -> {
				items.clear()
				notifyDataSetChanged()
			}
			is ListChangeNewItemAdded -> {
				notifyItemRangeInserted(items.size, change.newItems.size)
				items.addAll(change.newItems)
			}
		}
	}

	enum class ViewType(val typeId: Int) {
		PROGRESS(-2),
		ERROR(-1),
		MOVIE(1)
	}

	interface ItemClickListener {
		fun onMovieClicked(movie: MovieListItem)
		fun onRetryClicked()
	}

	class ProgressViewHolder(
		binding: ProgressItemBinding
	) : RecyclerView.ViewHolder(binding.root) {

		fun bind() {

		}
	}

	class ErrorViewHolder(
		private val binding: ErrorItemBinding
	) : RecyclerView.ViewHolder(binding.root) {

		fun bind(clickListener: ItemClickListener) {
			binding.root.setOnClickListener {
				clickListener.onRetryClicked()
			}
		}
	}

	class MovieViewHolder(
		private val binding: MovieItemBinding
	) : RecyclerView.ViewHolder(binding.root) {

		fun bind(movie: MovieListItem, clickListener: ItemClickListener) {
			binding.run {
				this.movie = movie
				this.clickListener = clickListener
				executePendingBindings()
			}
		}
	}
}

sealed class ListChange

object ListChangeCleared : ListChange()

data class ListChangeNewItemAdded(
	val newItems: List<MovieListItem>
) : ListChange()

