package com.github.aminmsvi.movio.utils

/**
 * @author aminmsvi
 * @since
 */

fun Float.map(inMin: Int, inMax: Int, outMin: Int, outMax: Int): Float {
	return (this - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
}