package com.github.aminmsvi.movio.model

import com.google.gson.annotations.SerializedName

/**
 * @author aminmsvi
 * @since
 */

typealias MovieId = Int

data class MovieListItem(
	@SerializedName("id") val id: MovieId = 0,
	@SerializedName("title") val title: String = "",
	@SerializedName("poster_path") val posterPath: String = "",
	@SerializedName("release_date") val releaseDate: String = "",
	@SerializedName("vote_average") val vote: Float = 0.0F
) {
	val thumbnail: String
		get() = "https://image.tmdb.org/t/p/w185/$posterPath"
}

data class Movie(
	@SerializedName("id") val id: MovieId = 0,
	@SerializedName("genres") val genres: List<Genre> = listOf(),
	@SerializedName("homepage") val homepage: String = "",
	@SerializedName("imdb_id") val imdbId: String = "",
	@SerializedName("overview") val overview: String = "",
	@SerializedName("poster_path") val posterPath: String = "",
	@SerializedName("release_date") val releaseDate: String = "",
	@SerializedName("title") val title: String = "",
	@SerializedName("vote_average") val vote: Float = 0.0F
) {
	val poster: String
		get() = "https://image.tmdb.org/t/p/w780$posterPath"

	val imdb: String
		get() = "https://www.imdb.com/title/$imdbId"

	data class Genre(
		@SerializedName("name") val name: String = ""
	)
}