package com.github.aminmsvi.movio.ui.moviedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.aminmsvi.movio.databinding.MovieDetailFragmentBinding
import com.github.aminmsvi.movio.model.MovieId
import com.github.aminmsvi.movio.ui.NavigationManager
import kotlinx.android.synthetic.main.movie_detail_fragment.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

/**
 * @author aminmsvi
 * @since
 */
class MovieDetailFragment : Fragment() {

	private val viewModel by viewModel<MovieDetailViewModel> {
		parametersOf(requireArguments().getInt("movie_id"))
	}
	private val navigationManager by lazy {
		requireActivity().lifecycleScope.inject<NavigationManager>()
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return MovieDetailFragmentBinding.inflate(inflater, container, false)
			.apply {
				viewModel = this@MovieDetailFragment.viewModel
				executePendingBindings()
			}.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		btn_back.setOnClickListener {
			navigationManager.value.navigateBack()
		}

		viewModel.fetchMovie()
	}

	companion object {
		fun newInstance(movieId: MovieId): MovieDetailFragment =
			MovieDetailFragment()
				.apply {
					arguments = Bundle().apply {
						putInt("movie_id", movieId)
					}
				}
	}
}