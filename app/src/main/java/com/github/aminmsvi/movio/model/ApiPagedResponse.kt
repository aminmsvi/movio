package com.github.aminmsvi.movio.model

import com.google.gson.annotations.SerializedName

/**
 * @author Amin
 * @since
 */
data class ApiPagedResponse<T>(
	@SerializedName("page") val page: Int,
	@SerializedName("total_results") val totalResults: Int,
	@SerializedName("total_pages") val totalPages: Int,
	@SerializedName("results") val results: List<T>
)