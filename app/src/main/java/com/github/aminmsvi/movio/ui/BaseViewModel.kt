package com.github.aminmsvi.movio.ui

import androidx.databinding.Observable
import androidx.databinding.Observable.OnPropertyChangedCallback
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * @author aminmsvi
 * @since
 */
open class BaseViewModel(
	protected val disposable: CompositeDisposable
) : ViewModel(), Observable {

	@Transient
	private var callbacks: PropertyChangeRegistry? = null

	override fun addOnPropertyChangedCallback(callback: OnPropertyChangedCallback) {
		synchronized(this) {
			if (callbacks == null) {
				callbacks = PropertyChangeRegistry()
			}
		}
		callbacks?.add(callback)
	}

	override fun removeOnPropertyChangedCallback(callback: OnPropertyChangedCallback) {
		synchronized(this) {
			if (callbacks == null) {
				return
			}
		}
		callbacks?.remove(callback)
	}

	override fun onCleared() {
		disposable.dispose()
		super.onCleared()
	}

	/**
	 * Notifies listeners that all properties of this instance have changed.
	 */
	fun notifyChange() {
		synchronized(this) {
			if (callbacks == null) {
				return
			}
		}
		callbacks?.notifyCallbacks(this, 0, null)
	}

	/**
	 * Notifies listeners that a specific property has changed. The getter for the property
	 * that changes should be marked with [Bindable] to generate a field in
	 * `BR` to be used as `fieldId`.
	 *
	 * @param fieldId The generated BR id for the Bindable field.
	 */
	fun notifyPropertyChanged(fieldId: Int) {
		synchronized(this) {
			if (callbacks == null) {
				return
			}
		}
		callbacks?.notifyCallbacks(this, fieldId, null)
	}
}