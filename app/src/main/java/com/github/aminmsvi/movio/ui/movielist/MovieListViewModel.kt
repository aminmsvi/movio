package com.github.aminmsvi.movio.ui.movielist

import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.databinding.library.baseAdapters.BR
import com.github.aminmsvi.movio.R
import com.github.aminmsvi.movio.dataaccess.MovieDataSource
import com.github.aminmsvi.movio.model.ApiPagedResponse
import com.github.aminmsvi.movio.model.MovieId
import com.github.aminmsvi.movio.model.MovieListItem
import com.github.aminmsvi.movio.ui.BaseViewModel
import com.github.aminmsvi.movio.utils.ResourceProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class MovieListViewModel(
	private val resourceProvider: ResourceProvider,
	disposable: CompositeDisposable,
	private val movieRepository: MovieDataSource
) : BaseViewModel(disposable), MovieAdapter.ItemClickListener {

	private val itemsChanged = PublishSubject.create<ListChange>()
	val onItemsChanged: Observable<ListChange>
		get() = itemsChanged.hide()

	private val fetchingNewItems = PublishSubject.create<Boolean>()
	val onFetchingNewItems: Observable<Boolean>
		get() = fetchingNewItems.hide()

	private val showSearchDialog = PublishSubject.create<Unit>()
	val onShowSearchDialog: Observable<Unit>
		get() = showSearchDialog.hide()

	private val hideSearchDialog = PublishSubject.create<Unit>()
	val onHideSearchDialog: Observable<Unit>
		get() = hideSearchDialog.hide()

	private val showMovieDetail = PublishSubject.create<MovieId>()
	val onShowMovieDetail: Observable<MovieId>
		get() = showMovieDetail.hide()

	private val errorInFetchingNewItems = PublishSubject.create<Boolean>()
	val onErrorInFetchingNewItems: Observable<Boolean>
		get() = errorInFetchingNewItems.hide()

	val searchQueryError = ObservableField<String>()
	val searchYearError = ObservableField<String>()

	@get:Bindable
	val state: State
		get() = when {
			movies.size == 0 && isErrorInFetchingNewItems -> State.ERROR
			totalResults == 0 -> State.EMPTY
			else -> State.LIST
		}

	private var totalResults = Int.MAX_VALUE
	private val movies = arrayListOf<MovieListItem>()
	private var stateHandler: MovieListStateHandler = PopularMoviesStateHandler(movieRepository)
	private var isErrorInFetchingNewItems = false
		set(value) {
			field = value
			errorInFetchingNewItems.onNext(value)
		}
	private var isFetchingNewItem = false
		set(value) {
			field = value
			fetchingNewItems.onNext(value)
		}

	override fun onMovieClicked(movie: MovieListItem) {
		showMovieDetail.onNext(movie.id)
	}

	override fun onCleared() {
		disposable.dispose()
		super.onCleared()
	}

	override fun onRetryClicked() {
		internalFetchMovies()
	}

	fun onSearchClick() {
		showSearchDialog.onNext(Unit)
	}

	fun performSearch(query: String, year: String) {
		if (validateSearchInputs(query, year)) {
			hideSearchDialog.onNext(Unit)
			changeState(SearchMoviesStateHandler(movieRepository, query, year.toIntOrNull() ?: -1))
		}
	}

	fun fetchPopularMovies() {
		changeState(PopularMoviesStateHandler(movieRepository))
	}

	fun fetchNextPage() {
		internalFetchMovies()
	}

	private fun validateSearchInputs(query: String, year: String): Boolean {
		var isValid = true

		searchQueryError.set(null)
		searchYearError.set(null)

		if (query.isEmpty()) {
			searchQueryError.set(resourceProvider.getString(R.string.movie_list_error_empty_title))
			isValid = false
		}

		if (year.isNotEmpty() && year.toIntOrNull() != null && year.length != 4) {
			searchYearError.set(resourceProvider.getString(R.string.movie_list_error_invalid_release_date))
			isValid = false
		}

		return isValid
	}

	private fun changeState(stateHandler: MovieListStateHandler) {
		this.stateHandler = stateHandler
		totalResults = Int.MAX_VALUE
		movies.clear()
		itemsChanged.onNext(ListChangeCleared)
		internalFetchMovies()
	}

	private fun internalFetchMovies() {
		if (isFetchingNewItem || movies.size >= totalResults) return
		disposable += stateHandler.fetchMovies()
			.doOnSubscribe {
				isErrorInFetchingNewItems = false
				isFetchingNewItem = true
				notifyPropertyChanged(BR.state)
			}
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.doFinally { notifyPropertyChanged(BR.state) }
			.subscribe({
				isFetchingNewItem = false
				totalResults = it.totalResults
				movies.addAll(it.results)
				itemsChanged.onNext(
					ListChangeNewItemAdded(
						newItems = it.results
					)
				)
			}, {
				isFetchingNewItem = false
				isErrorInFetchingNewItems = true
			})
	}

	enum class State {
		LIST, EMPTY, ERROR
	}
}

interface MovieListStateHandler {
	fun fetchMovies(): Single<ApiPagedResponse<MovieListItem>>
}

class PopularMoviesStateHandler(
	private val movieRepository: MovieDataSource
) : MovieListStateHandler {

	private var page = 1

	override fun fetchMovies(): Single<ApiPagedResponse<MovieListItem>> {
		return movieRepository.getPopular(page)
			.doOnSuccess { page++ }
	}
}

class SearchMoviesStateHandler(
	private val movieRepository: MovieDataSource,
	private val query: String,
	private val year: Int = -1
) : MovieListStateHandler {

	private var page = 1

	override fun fetchMovies(): Single<ApiPagedResponse<MovieListItem>> {
		return movieRepository.search(query, page, year)
			.doOnSuccess { page++ }
	}
}
