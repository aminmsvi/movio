package com.github.aminmsvi.movio.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Copied from https://gist.github.com/ssinss/e06f12ef66c51252563e
 * @author ssinss
 * @since
 */
abstract class EndlessRecyclerOnScrollListener(linearLayoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {
	private var previousTotal = 0 // The total number of items in the dataset after the last load
	private var loading = true // True if we are still waiting for the last set of data to load.
	private val visibleThreshold = 5 // The minimum amount of items to have below your current scroll position before loading more.
	private var firstVisibleItem = 0
	private var visibleItemCount = 0
	private var totalItemCount = 0
	private val mLinearLayoutManager: LinearLayoutManager = linearLayoutManager

	override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
		super.onScrolled(recyclerView, dx, dy)
		visibleItemCount = recyclerView.childCount
		totalItemCount = mLinearLayoutManager.itemCount
		firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()
		if (loading) {
			if (totalItemCount > previousTotal) {
				loading = false
				previousTotal = totalItemCount
			}
		}
		if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
			onLoadMore()
			loading = true
		}
	}

	abstract fun onLoadMore()
}