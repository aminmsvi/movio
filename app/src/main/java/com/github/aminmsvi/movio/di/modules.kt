package com.github.aminmsvi.movio.di

/**
 * @author aminmsvi
 * @since
 */
val modules = listOf(appModule, daModule, uiModule)