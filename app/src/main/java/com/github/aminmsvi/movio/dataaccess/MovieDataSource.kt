package com.github.aminmsvi.movio.dataaccess

import com.github.aminmsvi.movio.model.ApiPagedResponse
import com.github.aminmsvi.movio.model.Movie
import com.github.aminmsvi.movio.model.MovieListItem
import com.github.aminmsvi.movio.model.MovieId
import io.reactivex.Single

/**
 * @author aminmsvi
 * @since
 */
interface MovieDataSource {
	fun getPopular(page: Int): Single<ApiPagedResponse<MovieListItem>>
	fun search(query: String, page: Int, year: Int = -1): Single<ApiPagedResponse<MovieListItem>>
	fun getById(movieId: MovieId): Single<Movie>
}