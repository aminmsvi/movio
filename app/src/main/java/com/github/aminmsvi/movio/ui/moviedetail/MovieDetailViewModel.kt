package com.github.aminmsvi.movio.ui.moviedetail

import androidx.databinding.ObservableField
import com.github.aminmsvi.movio.dataaccess.MovieDataSource
import com.github.aminmsvi.movio.model.Movie
import com.github.aminmsvi.movio.model.MovieId
import com.github.aminmsvi.movio.ui.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers

/**
 * @author aminmsvi
 * @since
 */
class MovieDetailViewModel(
	disposable: CompositeDisposable,
	private val movieRepository: MovieDataSource,
	private val movieId: MovieId
) : BaseViewModel(disposable) {

	val movie = ObservableField(Movie())
	val state = ObservableField(State.LOADING)

	fun retry() {
		fetchMovie()
	}

	fun fetchMovie() {
		disposable += movieRepository.getById(movieId)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.doOnSubscribe { state.set(State.LOADING) }
			.subscribe({
				state.set(State.DATA)
				movie.set(it)
			}, {
				state.set(State.ERROR)
			})
	}

	enum class State {
		LOADING, ERROR, DATA
	}
}