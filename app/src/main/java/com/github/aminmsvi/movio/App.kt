package com.github.aminmsvi.movio

import androidx.multidex.MultiDexApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * @author aminmsvi
 * @since
 */
class App : MultiDexApplication() {

	override fun onCreate() {
		super.onCreate()
		initKoin()
	}

	private fun initKoin() {
		startKoin {
			androidContext(this@App)
			modules(com.github.aminmsvi.movio.di.modules)
		}
	}
}