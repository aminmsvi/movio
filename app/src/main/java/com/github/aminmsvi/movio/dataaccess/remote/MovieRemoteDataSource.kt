package com.github.aminmsvi.movio.dataaccess.remote

import com.github.aminmsvi.movio.model.ApiPagedResponse
import com.github.aminmsvi.movio.model.Movie
import com.github.aminmsvi.movio.model.MovieListItem
import com.github.aminmsvi.movio.model.MovieId
import io.reactivex.Single

/**
 * @author Amin
 * @since
 */
class MovieRemoteDataSource(
	private val apiService: ApiService
) {
	fun getPopular(page: Int): Single<ApiPagedResponse<MovieListItem>> = apiService.getPopular(page)

	fun getById(id: MovieId): Single<Movie> = apiService.getMovieById(id)

	fun search(query: String, page: Int, year: Int): Single<ApiPagedResponse<MovieListItem>> = apiService.searchMovies(query, page, if (year > 0) year else null)
}