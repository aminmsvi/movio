package com.github.aminmsvi.movio.di

import com.github.aminmsvi.movio.BuildConfig
import com.github.aminmsvi.movio.R
import com.github.aminmsvi.movio.dataaccess.remote.ApiService
import com.github.aminmsvi.movio.utils.AuthInterceptor
import com.github.aminmsvi.movio.utils.ResourceProvider
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author aminmsvi
 * @since
 */
val appModule = module {

	factory { CompositeDisposable() }

	single { ResourceProvider(androidContext()) }

	single { Gson() }

	single {
		val builder = OkHttpClient.Builder()
			.addInterceptor(AuthInterceptor(androidApplication().getString(R.string.API_KEY)))

		if (BuildConfig.DEBUG) {
			val interceptor = HttpLoggingInterceptor().apply {
				setLevel(HttpLoggingInterceptor.Level.BODY)
			}
			builder.addInterceptor(interceptor)
		}

		builder.build()
	}

	single {
		Retrofit.Builder()
			.baseUrl("https://api.themoviedb.org/")
			.client(get())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.addConverterFactory(GsonConverterFactory.create(get()))
			.build()
			.create(ApiService::class.java)
	}
}