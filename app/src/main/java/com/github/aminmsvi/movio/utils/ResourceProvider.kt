package com.github.aminmsvi.movio.utils

import android.content.Context

/**
 * @author aminmsvi
 * @since
 */
class ResourceProvider(
	private val context: Context
) {
	fun getString(resId: Int): String = context.getString(resId)
}