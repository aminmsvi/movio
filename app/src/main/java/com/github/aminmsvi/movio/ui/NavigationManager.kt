package com.github.aminmsvi.movio.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

class NavigationManager(
	private val fragmentManager: FragmentManager,
	private val container: Int
) {

	fun open(fragment: Fragment) {
		openFragment(fragment, addToBackStack = true, isRoot = false)
	}

	private fun openFragment(fragment: Fragment, addToBackStack: Boolean, isRoot: Boolean) {
		val fragTransaction = fragmentManager.beginTransaction()

		if (isRoot) {
			fragTransaction.add(container, fragment, "ROOT")
		} else {
			fragTransaction.add(container, fragment)
		}

		fragTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
		if (addToBackStack)
			fragTransaction.addToBackStack(fragment.toString())
		fragTransaction.commit()
	}

	fun openAsRoot(fragment: Fragment) {
		popEveryFragment()
		openFragment(fragment, addToBackStack = false, isRoot = true)
	}

	private fun popEveryFragment() {
		fragmentManager.popBackStackImmediate("ROOT", FragmentManager.POP_BACK_STACK_INCLUSIVE)
	}

	fun navigateBack(): Boolean {
		return if (fragmentManager.backStackEntryCount == 0) {
			false
		} else {
			fragmentManager.popBackStackImmediate()
			true
		}
	}
}