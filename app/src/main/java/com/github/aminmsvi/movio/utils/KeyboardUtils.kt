package com.github.aminmsvi.movio.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * @author aminmsvi
 * @since 1.0
 */

fun Context?.showSoftInput(view: View) {
	if (this == null) return
	view.requestFocus()
	(getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.showSoftInput(view, 0)
}
