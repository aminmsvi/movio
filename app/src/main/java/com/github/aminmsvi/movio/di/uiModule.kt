package com.github.aminmsvi.movio.di

import androidx.fragment.app.FragmentManager
import com.github.aminmsvi.movio.model.MovieId
import com.github.aminmsvi.movio.ui.MainActivity
import com.github.aminmsvi.movio.ui.NavigationManager
import com.github.aminmsvi.movio.ui.moviedetail.MovieDetailViewModel
import com.github.aminmsvi.movio.ui.movielist.MovieListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * @author aminmsvi
 * @since
 */
val uiModule = module {

	scope<MainActivity> {
		scoped { (fragmentManager: FragmentManager, containerId: Int) ->
			NavigationManager(fragmentManager, containerId)
		}
	}

	viewModel {
		MovieListViewModel(
			resourceProvider = get(),
			disposable = get(),
			movieRepository = get()
		)
	}

	viewModel { (movieId: MovieId) ->
		MovieDetailViewModel(
			disposable = get(),
			movieRepository = get(),
			movieId = movieId
		)
	}
}