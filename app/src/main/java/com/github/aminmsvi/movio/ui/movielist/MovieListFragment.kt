package com.github.aminmsvi.movio.ui.movielist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.aminmsvi.movio.databinding.MovieListFragmentBinding
import com.github.aminmsvi.movio.ui.NavigationManager
import com.github.aminmsvi.movio.ui.moviedetail.MovieDetailFragment
import com.github.aminmsvi.movio.utils.EndlessRecyclerOnScrollListener
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.movie_list_fragment.*
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieListFragment : Fragment() {

	private val viewModel by viewModel<MovieListViewModel>()
	private val disposable by inject<CompositeDisposable>()
	private val navigationManager by lazy {
		requireActivity().lifecycleScope.inject<NavigationManager>()
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		return MovieListFragmentBinding.inflate(inflater, container, false)
			.apply {
				viewModel = this@MovieListFragment.viewModel
				executePendingBindings()
			}.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		rv_movies.run {
			adapter = MovieAdapter(viewModel)
			addOnScrollListener(object : EndlessRecyclerOnScrollListener(layoutManager as LinearLayoutManager) {
				override fun onLoadMore() {
					viewModel.fetchNextPage()
				}
			})
		}

		observerViewModel()

		viewModel.fetchPopularMovies()
	}

	override fun onDestroyView() {
		disposable.clear()
		super.onDestroyView()
	}

	private fun observerViewModel() {
		disposable += viewModel.onItemsChanged.subscribe {
			(rv_movies.adapter as MovieAdapter).submitChange(it)
		}

		disposable += viewModel.onFetchingNewItems.subscribe {
			if (it) {
				(rv_movies.adapter as MovieAdapter).addLoading()
			} else {
				(rv_movies.adapter as MovieAdapter).removeLoading()
			}
		}

		disposable += viewModel.onShowSearchDialog.subscribe {
			MovieListSearchFragment.newInstance()
				.show(childFragmentManager, "movie_list_search_fragment")
		}

		disposable += viewModel.onHideSearchDialog.subscribe {
			val fragment = childFragmentManager.findFragmentByTag("movie_list_search_fragment") as? DialogFragment
			fragment?.dismiss()
		}

		disposable += viewModel.onShowMovieDetail.subscribe {
			navigationManager.value.open(MovieDetailFragment.newInstance(it))
		}

		disposable += viewModel.onErrorInFetchingNewItems.subscribe {
			if (it) {
				(rv_movies.adapter as MovieAdapter).showError()
			} else {
				(rv_movies.adapter as MovieAdapter).hideError()
			}
		}
	}

	companion object {
		fun newInstance() = MovieListFragment()
	}
}
