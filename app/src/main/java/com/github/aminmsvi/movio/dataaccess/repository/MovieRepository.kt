package com.github.aminmsvi.movio.dataaccess.repository

import com.github.aminmsvi.movio.dataaccess.MovieDataSource
import com.github.aminmsvi.movio.dataaccess.remote.MovieRemoteDataSource
import com.github.aminmsvi.movio.model.ApiPagedResponse
import com.github.aminmsvi.movio.model.Movie
import com.github.aminmsvi.movio.model.MovieId
import com.github.aminmsvi.movio.model.MovieListItem
import io.reactivex.Single

/**
 * @author aminmsvi
 * @since
 */
class MovieRepository(
	private val remote: MovieRemoteDataSource
) : MovieDataSource {

	override fun getPopular(page: Int): Single<ApiPagedResponse<MovieListItem>> {
		// TODO maybe use room to cache data later
		return remote.getPopular(page)
	}

	override fun search(query: String, page: Int, year: Int): Single<ApiPagedResponse<MovieListItem>> = remote.search(query, page, year)

	override fun getById(movieId: MovieId): Single<Movie> = remote.getById(movieId)
}