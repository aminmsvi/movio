package com.github.aminmsvi.movio.widget

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class TopCropImageView @JvmOverloads constructor(
	context: Context,
	attrs: AttributeSet? = null,
	defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

	init {
		scaleType = ScaleType.MATRIX
	}

	override fun setImageBitmap(bm: Bitmap?) {
		super.setImageBitmap(bm)
		val matrix = imageMatrix
		val scaleRatio = (resources.displayMetrics.widthPixels / drawable.intrinsicWidth).toFloat()
		matrix.postScale(scaleRatio, scaleRatio);
		imageMatrix = matrix
	}
}