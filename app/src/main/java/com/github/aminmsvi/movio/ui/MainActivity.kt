package com.github.aminmsvi.movio.ui

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.github.aminmsvi.movio.R
import com.github.aminmsvi.movio.ui.movielist.MovieListFragment
import org.koin.androidx.scope.lifecycleScope
import org.koin.core.parameter.parametersOf


class MainActivity : AppCompatActivity() {

	private val navigationManager by lifecycleScope.inject<NavigationManager> {
		parametersOf(supportFragmentManager, R.id.container)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.main_activity)

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
		}

		if (savedInstanceState == null) {
			navigationManager.openAsRoot(MovieListFragment.newInstance())
		}
	}
}
