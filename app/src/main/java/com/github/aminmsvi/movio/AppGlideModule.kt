package com.github.aminmsvi.movio

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * @author aminmsvi
 * @since
 */
@GlideModule
class AppGlideModule : AppGlideModule()