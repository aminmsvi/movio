package com.github.aminmsvi.movio.di

import com.github.aminmsvi.movio.dataaccess.MovieDataSource
import com.github.aminmsvi.movio.dataaccess.remote.MovieRemoteDataSource
import com.github.aminmsvi.movio.dataaccess.repository.MovieRepository
import org.koin.dsl.module

/**
 * @author aminmsvi
 * @since
 */
val daModule = module {
	single {
		MovieRemoteDataSource(
				apiService = get()
		)
	}
	single<MovieDataSource> {
		MovieRepository(
				remote = get()
		)
	}
}