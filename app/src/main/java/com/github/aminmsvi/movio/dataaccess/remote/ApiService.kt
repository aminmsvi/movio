package com.github.aminmsvi.movio.dataaccess.remote

import com.github.aminmsvi.movio.model.ApiPagedResponse
import com.github.aminmsvi.movio.model.Movie
import com.github.aminmsvi.movio.model.MovieId
import com.github.aminmsvi.movio.model.MovieListItem
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author aminmsvi
 * @since
 */
interface ApiService {

	@GET("3/movie/popular")
	fun getPopular(@Query("page") page: Int): Single<ApiPagedResponse<MovieListItem>>

	@GET("3/search/movie")
	fun searchMovies(@Query("query") query: String, @Query("page") page: Int, @Query("year") year: Int?): Single<ApiPagedResponse<MovieListItem>>

	@GET("3/movie/{movie_id}")
	fun getMovieById(@Path("movie_id") id: MovieId): Single<Movie>
}