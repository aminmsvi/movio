package com.github.aminmsvi.movio.ui.movielist

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.github.aminmsvi.movio.R
import com.github.aminmsvi.movio.databinding.MovieListSearchFragmentBinding
import kotlinx.android.synthetic.main.movie_list_search_fragment.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

/**
 * @author aminmsvi
 * @since
 */
class MovieListSearchFragment : DialogFragment() {

	private val viewModel by lazy {
		requireParentFragment().getViewModel<MovieListViewModel>()
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return MovieListSearchFragmentBinding.inflate(inflater, container, false)
			.apply {
				viewModel = this@MovieListSearchFragment.viewModel
				executePendingBindings()
			}.root
	}

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		return Dialog(requireContext(), R.style.MovieListSearchDialogStyle)
			.apply {
				window?.run {
					setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
					attributes.windowAnimations = R.style.DialogBoingAnimation
					setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
				}
			}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		btn_cancel.setOnClickListener {
			dismiss()
		}
	}

	companion object {
		fun newInstance() = MovieListSearchFragment()
	}
}